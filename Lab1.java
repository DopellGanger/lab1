import java.util.Scanner;

public class Lab1 {
  public static void main(String[] args) {
    System.out.println("1) prime");
    System.out.println("2) palindrom");
    Scanner sc = new Scanner(System.in);
    int i = sc.nextInt();
    if (i == 1)
      prime();
    if (i == 2)
      palindrome();
  }

  private static void prime() {
    int n = readInt();
    while (n <= 1) {
      System.out.println("Number greater than 1 is required");
      n = readInt();
    }
    System.out.print("1 ");
    for (int i = 2; i <= n; i++)
      if (isPrime(i))
        System.out.print(i + " ");
    System.out.print("\n");
  }

  private static void palindrome() {
    System.out.println(isPalindrome(readStr()) ? "+" : "-");
  }

  private static int readInt() {
    Scanner sc = new Scanner(System.in);
    int n = sc.nextInt();
    return n;
  }

  private static String readStr() {
    Scanner sc = new Scanner(System.in);
    String str = sc.nextLine();
    return str;
  }

  private static boolean isPrime(int n) {
    for (int i = 2; i < n; i++)
      if (n % i == 0)
        return false;
    return true;
  }

  private static boolean isPalindrome(String str) {
    int c;
    if (str.length() % 2 == 0)
      c = str.length() / 2;
    else
      c = (str.length() - 1) / 2;

    for (int i = 0; i <= c; i++)
      if (str.charAt(i) != str.charAt(str.length() - 1 - i))
        return false;
    return true;
  }

}
